using UnityEngine;

namespace YsoCorp {

    [DefaultExecutionOrder(-1)]
    public class ResourcesManager : AResourcesManager {

        public Map forceMap;
        public int ignoreFirstMapRandom = 0;

        public Map mapLast { get; set; } = null;

        private Map[] _maps;

        protected override void Awake() {
            base.Awake();
            this._maps = this.LoadIterator<Map>("Maps/Map");
        }

        public Map GetMap() {
#if UNITY_EDITOR
            if (this.forceMap != null) {
                return this.forceMap;
            }
#endif
            if (this.mapLast != null) {
                return this.mapLast;
            } 
            int level = this.dataManager.GetLevel() - 1;
            if (level < this._maps.Length) {
                this.mapLast = this._maps[level % this._maps.Length];
            } else {
                this.mapLast = this._maps[Random.Range(this.ignoreFirstMapRandom, this._maps.Length)];
            }
            return this.mapLast;
        }

    }

}