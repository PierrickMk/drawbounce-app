﻿using DG.Tweening;
using UnityEngine;

namespace YsoCorp
{
    public class Draw : YCBehaviour
    {
        public MeshRenderer _mesh;
        public Material defaultMat;
        public Material fadeMat;
        public DOTweenAnimation anim;

        public void SetFade(bool fade)
        {
            _mesh.material = fade ? fadeMat : defaultMat;
        }
        public void Pop()
        {
            anim.DORewind();
            anim.DOPlay();
        }
    }
}