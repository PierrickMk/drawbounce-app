﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace YsoCorp
{
    public class GamePool : YCSingleton<GamePool>
    {
        [SerializeField] private Pool[] _pools;
        private Dictionary<string, Pool> _poolDic = new Dictionary<string, Pool>();

        public GameObject GetObject(string name)
        {
            return this._poolDic[name].GetObject();
        }
        public static void SetupObject(GameObject obj, Transform transform)
        {
            obj.transform.position = transform.position;
            obj.transform.rotation = transform.rotation;
        }
        public static void SetupObject(GameObject obj, Vector3 position, Quaternion rotation)
        {
            obj.transform.position = position;
            obj.transform.rotation = rotation;
        }

        private void Start()
        {
            foreach(var pool in this._pools)
            {
                this._poolDic.Add(pool.Name, pool);
            }
        }
    }

    [System.Serializable]
    public class Pool
    {
        [SerializeField] private string name;
        [SerializeField] private GameObject prefab;
        private List<GameObject> pool = new List<GameObject>();

        public string Name { get => name; }

        public GameObject GetObject()
        {
            foreach (var obj in pool)
            {
                if (obj.activeSelf == false)
                {
                    obj.SetActive(true);
                    return obj;
                }
            }

            GameObject instance = Object.Instantiate(prefab);
            pool.Add(instance);
            return instance;
        }
    }
}