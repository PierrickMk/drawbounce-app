﻿using System;
using UnityEngine;
using UnityEngine.Animations;

namespace YsoCorp
{
    public class BulletLauncher : YCBehaviour
    {
        [SerializeField] private string bulletName = "ball";
        [SerializeField] private Transform spawn;
        [SerializeField] private float rate;
        [SerializeField] private float speed;
        [SerializeField] private bool launchOnStart;
        private float launchTime;

        public bool Launch { get; set; }

        public event Action<GameObject> OnLaunch;

        public void SetRate(float rate) => this.rate = rate;

        private void Start()
        {
            if (launchOnStart) Launch = true;
        }

        private void Update()
        {
            if (Launch == false) return;

            if (Time.time - launchTime < rate) return;
            launchTime = Time.time;

            GameObject instance = GamePool.Instance.GetObject(bulletName);
            GamePool.SetupObject(instance, spawn);

            instance.GetComponent<Rigidbody>().velocity = instance.transform.forward * speed;

            OnLaunch?.Invoke(instance);
        }
    }
}