﻿using DG.Tweening;
using UnityEngine;

namespace YsoCorp
{
    public class Multiplier : YCBehaviour
    {
        [SerializeField] private float spaceSize = 1;
        [SerializeField] private float forwardPos = 0;
        [SerializeField] [Range(2, 5)] private int count = 1;
        [SerializeField] private Factor[] factors;
        [SerializeField] private DOTweenAnimation anim;

        [System.Serializable]
        public class Factor
        {
            public int factor;
            public Vector3[] offsets;
        }

        private void OnTriggerEnter(Collider other)
        {
            if(other.CompareTag("Ball"))
            {
                Ball ball = other.GetComponent<Ball>();

                if (ball.LastMultiplier == this) return;

                //if (!other.GetComponent<Ball>().CanMultiply) return;

                Rigidbody rb = other.GetComponent<Rigidbody>();
                Vector3 dir = rb.velocity.normalized;
                Vector3 right = Vector3.Cross(dir, Vector3.up);
                //Vector3 offset = right * spaceSize;

                //V1
                //CreateBall(other.transform.position + offset + (dir * forwardPos), other.transform.rotation, rb.velocity);
                //CreateBall(other.transform.position - offset + (dir * forwardPos), other.transform.rotation, rb.velocity);
                //other.gameObject.SetActive(false);

                //V2
                //float randomSize = 0.5f;
                //for (int i = 0; i < factor; i++)
                //{
                //    CreateBall(other.transform.position + new Vector3(Random.Range(-randomSize, randomSize), 0, Random.Range(-randomSize, randomSize)), other.transform.rotation, rb.velocity);
                //}

                //V3
                for (int i = 0; i < factors.Length; i++)
                {
                    if(factors[i].factor == count)
                    {
                        for (int j = 0; j < factors[i].offsets.Length; j++)
                        {
                            Vector3 offset = factors[i].offsets[j];
                            Matrix4x4 m = Matrix4x4.TRS(other.transform.position, Quaternion.LookRotation(dir), Vector3.one);
                            Vector3 worldPos = m.MultiplyPoint3x4(offset);
                            CreateBall(worldPos, other.transform.rotation, rb.velocity);
                        }
                    }
                }

                other.GetComponent<Ball>().MultiplyCount++;

                anim.DORewind();
                anim.DOPlay();
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("Ball"))
            {
                Ball ball = other.GetComponent<Ball>();

                //Cannot multiply more than once
                //if (ball.MultiplyCount >= 1 || ball.LastMultiplier == this) return;

                //ball.CanMultiply = true;

                ball.SetMultiplier(this);
            }
        }

        private void CreateBall(Vector3 position, Quaternion rotation, Vector3 velocity)
        {
            GameObject instance = GamePool.Instance.GetObject("ball");
            GamePool.SetupObject(instance, position, rotation);
            Rigidbody rb = instance.GetComponent<Rigidbody>();
            rb.velocity = velocity;

            Ball ball = instance.GetComponent<Ball>();
            ball.SetMultiplier(this);
        }
    }
}