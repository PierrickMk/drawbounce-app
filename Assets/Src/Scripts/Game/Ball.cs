﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace YsoCorp
{
    public class Ball : MonoBehaviour
    {
        [SerializeField] private TrailRenderer trail;
        [SerializeField] private TrailAnim trailAnim;
        [SerializeField] private Color winColor;
        [SerializeField] private Color loseColor;
        [SerializeField] private DOTweenAnimation anim;
        public bool CanMultiply { get; set; }
        public int MultiplyCount { get; set; }
        public Multiplier LastMultiplier { get; private set; }

        public void SetMultiplier(Multiplier multiplier) => LastMultiplier = multiplier;

        public void Hide()
        {
            trailAnim.Play(false);
            StartCoroutine(HideProcess());
        }
        private IEnumerator HideProcess()
        {
            yield return new WaitForSeconds(0.2f);
            gameObject.SetActive(false);
        }

        private void OnDisable()
        {
            LastMultiplier = null;
            CanMultiply = false;
            MultiplyCount = 0;

            trail.Clear();
            anim.DORewind();
        }

        private void OnEnable()
        {
            StartCoroutine(PlayAnim());
        }

        private IEnumerator PlayAnim()
        {
            yield return new WaitForEndOfFrame();
            anim.DOPlay();
            trailAnim.Play(true);
        }

        private void OnTriggerExit(Collider other)
        {
            if(other.CompareTag("Boundary"))
            {
                gameObject.SetActive(false);
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Goal"))
            {
                Goal goal = other.GetComponent<Goal>();

                if(!goal.Broken) 
                {
                    gameObject.SetActive(false);
                    Pop(winColor);
                }
                
                GameController.Instance.AddScore();
            }

            if (other.CompareTag("Obstacle"))
            {
                gameObject.SetActive(false);
                Pop(loseColor);
            }
        }

        private void OnCollisionEnter(Collision collision)
        {
            GameObject vfx = GamePool.Instance.GetObject("vfx_bounce");
            vfx.transform.position = collision.contacts[0].point;
            vfx.transform.rotation = Quaternion.LookRotation(collision.contacts[0].normal);

            if (collision.transform.CompareTag("Draw")) collision.transform.GetComponentInParent<Draw>().Pop();
        }

        private void Pop(Color color)
        {
            GameObject vfx = GamePool.Instance.GetObject("vfx_pop");
            vfx.GetComponent<VFX_Color>().SetColor(color);
            GamePool.SetupObject(vfx, transform);
        }


        //private void OnCollisionEnter(Collision collision)
        //{
        //    //Vector3 dir = Vector3.Reflect((collision.contacts[0].point - transform.position).normalized, collision.contacts[0].normal);
        //    //rb.velocity = dir * speed;
        //}
    }
}