﻿using DigitalRubyShared;
using UnityEngine;

namespace YsoCorp
{
    public class Drawer : YCBehaviour
    {
        public GameObject draw;
        public float floorHeight = 1f;
        public float scale;
        public Transform finger;
        public float fingerHeight;
        private PanGestureRecognizer _pan;
        private Collider _currentCol;
        private Draw _currentDraw;

        private void Start()
        {
            InitPan();

            finger.gameObject.SetActive(false);
        }

        private void InitPan()
        {
            _pan = new PanGestureRecognizer();
            _pan.ThresholdUnits = 0f;
            _pan.StateUpdated += (GestureRecognizer gesture) => {
                if (this.game.state != Game.States.Playing)
                {
                    return;
                }
                if (_pan.State == GestureRecognizerState.Began)
                {         
                    StartDraw();
                }
                else if (_pan.State == GestureRecognizerState.Executing)
                {
                    Draw();
                }
                else if (_pan.State == GestureRecognizerState.Ended)
                {
                    CheckRemoveDraw();
                    EndDraw();
                }
            };
            FingersScript.Instance.AddGesture(_pan);
            FingersScript.Instance.ShowTouches = false;
        }

        private void Draw()
        {
            Vector3 point = GetTouchPoint();
            finger.position = new Vector3(point.x, fingerHeight, point.z);

            float dist = Vector3.Distance(_currentDraw.transform.position, point);

            if (dist < 1) return;

            _currentDraw.transform.localScale = new Vector3(1, 1, dist * scale);

            Vector3 dir = (point - _currentDraw.transform.position).normalized;

            if(dir != Vector3.zero) _currentDraw.transform.rotation = Quaternion.LookRotation(dir);
        }

        private void StartDraw()
        {
            Vector3 point = GetTouchPoint();
            GameObject instance = Instantiate(draw, point, Quaternion.identity);
            _currentDraw = instance.GetComponent<Draw>();
            _currentCol = _currentDraw.GetComponentInChildren<Collider>();
            _currentCol.isTrigger = true;
            _currentDraw.SetFade(true);
            finger.gameObject.SetActive(true);
            finger.position = new Vector3(point.x, fingerHeight, point.z);
        }

        private void EndDraw()
        {
            finger.gameObject.SetActive(false);

            if (_currentDraw.transform.localScale.z <= 1)
            {
                Destroy(_currentDraw.gameObject);
                return;
            }

            _currentCol.isTrigger = false;
            _currentDraw.SetFade(false);
            _currentDraw.Pop();
        }

        private Vector3 GetTouchPoint()
        {
            Ray ray = cam.ycCamera.ScreenPointToRay(MousePos);
            RaycastHit hit;
            Vector3 point = Vector3.zero;
            Physics.Raycast(ray, out hit);

            if (hit.transform != null) point = new Vector3(hit.point.x, floorHeight, hit.point.z);

            return point;
        }

        private Vector2 MousePos => new Vector2(_pan.FocusX, _pan.FocusY);

        private void CheckRemoveDraw()
        {
            Ray ray = cam.ycCamera.ScreenPointToRay(MousePos);
            RaycastHit hit;
            if(Physics.Raycast(ray, out hit, 100f, ~0, QueryTriggerInteraction.Ignore))
            {
                if (hit.transform.CompareTag("Draw"))
                {
                    Destroy(hit.transform.gameObject);
                }
            }
        }
    }
}