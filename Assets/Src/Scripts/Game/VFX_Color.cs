﻿using UnityEngine;

namespace YsoCorp
{
    public class VFX_Color : YCBehaviour
    {
        [SerializeField] private ParticleSystem[] particles;

        public void SetColor(Color color)
        {
            foreach(var p in particles)
            {
                var m = p.main;
                m.startColor = color;
            }
        }
    }
}