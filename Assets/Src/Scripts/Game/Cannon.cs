﻿using DG.Tweening;
using UnityEngine;

namespace YsoCorp
{
    public class Cannon : YCBehaviour
    {
        [SerializeField] private DOTweenAnimation anim;

        private void Start()
        {
            BulletLauncher b = GetComponent<BulletLauncher>();
            b.OnLaunch += (o) => { PlayAnim(); };
        }

        private void PlayAnim()
        {
            anim.DORewind();
            anim.DOPlay();
        }
    }
}