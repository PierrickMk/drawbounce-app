﻿using UnityEngine;

namespace YsoCorp
{
    public class AutoDisable : YCBehaviour
    {
        [SerializeField] private float _delay = 10;

        private void OnEnable()
        {
            Invoke("Disable", this._delay);
        }

        private void Disable()
        {
            this.gameObject.SetActive(false);
        }
    }
}