﻿using DG.Tweening;
using System.Collections;
using TMPro;
using UnityEngine;

namespace YsoCorp
{
    public class GameController : YCSingleton<GameController>
    {
        [SerializeField] private int scoreToWin = 100;
        [SerializeField] private int ballCount = 30;
        [SerializeField] private int ballBonus = 10;
        [SerializeField] private float cameraHeight = 20;
        [SerializeField] private float startLaunchRate = 1;
        [SerializeField] private float maxLaunchRate = 0.1f;
        [SerializeField] private AnimationCurve launchCurve;
        [SerializeField] private TextMeshProUGUI scoreText;
        [SerializeField] private TextMeshProUGUI ballText;
        [SerializeField] private DOTweenAnimation anim;
        private BulletLauncher bulletLauncher;
        private int score;
        private bool win;
        private int startBallCount;

        public void AddScore()
        {
            score++;
            UpdateScore();

            if (score >= scoreToWin && win == false)
            {
                win = true;
                //game.state = Game.States.Win;
                Goal.Instance.Break();
                StartCoroutine(WinDelay());
            }

            anim.DORewind();
            anim.DOPlay();
        }
        private IEnumerator WinDelay()
        {
            yield return new WaitForSeconds(3);
            game.state = Game.States.Win;
        }

        private void Start()
        {
            game.onStateChange.AddListener(Play);

            startBallCount = ballCount;

            UpdateBall();
            UpdateScore();

            Camera cam = FindObjectOfType<Camera>();
            cam.transform.position = new Vector3(0, cameraHeight, 0);

            StartCoroutine(StartDelay());
        }
        private IEnumerator StartDelay()
        {
            yield return new WaitForEndOfFrame();

            bulletLauncher = FindObjectOfType<BulletLauncher>();
            bulletLauncher.OnLaunch += BallLaunch;
            UpdateLaunchRate();
        }

        private void UpdateScore() => scoreText.text = score.ToString() + "/" + scoreToWin;
        private void UpdateBall() => ballText.text = (ballCount + ballBonus).ToString();

        private void Play(Game.States state)
        {
            if (state != Game.States.Playing) return;

            bulletLauncher.Launch = true;
        }

        private void BallLaunch(GameObject obj)
        {
            obj.GetComponent<Ball>().CanMultiply = true;

            if(ballCount > 0)
            {
                ballCount--;
                UpdateLaunchRate();
            }
            else
            {
                ballBonus--;
            }

            UpdateBall();

            if (ballCount <= 0 && ballBonus <= 0)
            {
                bulletLauncher.Launch = false;
                StartCoroutine(EndDelay());
            }
        }

        private void UpdateLaunchRate()
        {
            float ratio = (1 - (float)ballCount / startBallCount);
            float launchRate = launchCurve.Evaluate(ratio);
            bulletLauncher.SetRate(launchRate);
        }

        private IEnumerator EndDelay()
        {
            yield return new WaitForSeconds(10);

            if (!win) game.state = Game.States.Lose;
        }
    }
}