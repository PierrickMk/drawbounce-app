﻿using DG.Tweening;
using UnityEngine;

namespace YsoCorp
{
    public class Goal : YCSingleton<Goal>
    {
        [SerializeField] private Color winColor;
        [SerializeField] private float explosionScale = 2;
        [SerializeField] private float randomScale = 1;
        public DOTweenAnimation anim;
        public GameObject piecesPrefab;
        public GameObject model;

        public bool Broken { get; private set; }

        public void Break()
        {
            for (int i = 0; i < 5; i++)
            {
                GameObject vfx = GamePool.Instance.GetObject("vfx_pop");
                vfx.GetComponent<VFX_Color>().SetColor(winColor);
                GamePool.SetupObject(vfx, transform.position + new Vector3(Random.Range(-randomScale, randomScale), 0, Random.Range(-randomScale, randomScale)), transform.rotation);
                vfx.transform.localScale *= explosionScale;
            }

            Instantiate(piecesPrefab, transform.position, transform.rotation);
            model.SetActive(false);
            Broken = true;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.transform.CompareTag("Ball"))
            {
                PlayAnim();
            }
        }

        private void PlayAnim()
        {
            anim.DORewind();
            anim.DOPlay();
        }
    }
}