﻿using System.Collections;
using UnityEngine;

namespace YsoCorp
{
    [RequireComponent(typeof(TrailRenderer))]
    public class TrailAnim : YCBehaviour
    {
        [SerializeField] private TrailRenderer trail;
        [SerializeField] private float duration = 0.5f;
        [SerializeField] private float trailSize = 1;
        [SerializeField] private AnimationCurve fadeInCurve;

        public void Play(bool fadeIn)
        {
            if(fadeIn)
            {
                StartCoroutine(FadeIn(duration));
            }
            else
            {
                StartCoroutine(FadeOut(duration));
            }
        }

        private IEnumerator FadeIn(float duration)
        {
            float t = duration;
            float speed = trailSize / duration;
            float target = 0;
            trail.startWidth = target;

            while (t > 0)
            {
                target += speed * Time.deltaTime;
                trail.startWidth = fadeInCurve.Evaluate(target);
                t -= Time.deltaTime;
                yield return null;
            }

            trail.startWidth = trailSize;
        }

        private IEnumerator FadeOut(float duration)
        {
            float t = duration;
            float speed = trailSize / duration;
            trail.startWidth = trailSize;

            while (t > 0)
            {
                trail.startWidth -= speed * Time.deltaTime;
                t -= Time.deltaTime;
                yield return null;
            }

            trail.startWidth = 0;
        }
    }
}